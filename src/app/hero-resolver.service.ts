import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';
import { Hero } from './hero';
import { HeroService }  from './hero.service';


@Injectable()
export class HeroResolver implements Resolve<Observable<Hero>> {
  constructor(private heroService: HeroService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Hero> {
    const id = +route.paramMap.get('id');
    return this.heroService.getHero(id).pipe(delay(3000));
  }
}
